class Utils {

	static getDirImagens() {
		let _dirImagens = '';
		if (!_dirImagens) {
			if (process.env.NODE_ENV == 'production') {				
				_dirImagens = '/root/Barbearia/images';
			} else {
				_dirImagens = 'E:/Adrian/Alura/JavaScript/Barbearia/images';
			}
		}
		
		return _dirImagens;
	}
}

module.exports = Utils;