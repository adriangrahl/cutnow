var http = require('http');
var fs = require('fs');

var configuracoes = {
  hostname : 'localhost',
  port : 8000,
  path : '/portfolio',
  method : 'get',
  headers: {
    'x-access-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI2IiwiZXhwIjoxNDkyODQxNDk1ODQyfQ.oVVOsrpCajj-ceussQ3A_3TdzB9ecgCSVUMIQslrWvc'
  }
}

var client = http.request(configuracoes,function(res){

  res.pipe(
     fs.createWriteStream(res.headers.filename)
  )
  .on('finish',function() {
    console.log('acabou, BIRL!');
   });
});

client.end();
