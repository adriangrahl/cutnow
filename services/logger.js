var winston = require('winston');
var MongoDB = require('winston-mongodb').MongoDB;
var fs = require('fs');

if (!fs.existsSync('logs')){
	fs.mkdirSync('logs');
}

fs.writeFile('logs/teste.log', '', function(err) {
	if (err) throw err;
	fs.close
});

//info, debug, error
/*module.exports = new winston.Logger({
	transports:[
		new winston.transports.File({
			level: "info",
			db: ''
			filename: "logs/teste.log",
			maxsize: 10000,
			maxFiles: 10
		})
	]
});*/

var logger = new (winston.Logger)({
    transports: [
        new(winston.transports.MongoDB)({
            //db : 'mongodb://localhost:27017/Book-catalog',
            //db : 'mongodb://admin:123456@127.0.0.1:27017/barbearia?authSource=admin',
            db : 'mongodb://127.0.0.1:27017/barbearia',
            collection: 'logs'
            //level : process.env.LOG_LEVEL (instalar dotenv)
        })
       ]
    });
//{ error: 0, warn: 1, info: 2, verbose: 3, debug: 4, silly: 5 }

//uso:
//logger.info('barbearias","INFO');
//logger.debug('barbearias","DEBUG');
//logger.silly('barbearias","SILLY');
//logger.error('barbearias","ERRO'); CATCH
//logger.debug('barbearias","Recebida requisição de consulta de barbearias.","'+usuario.id);

module.exports = logger;
