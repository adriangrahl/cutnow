var //mongoose = require('mongoose'),
    Document = require('camo').Document,
    Barbearia = require('./barbeariaModel'),
    ObjectId = require('mongodb').ObjectId,
    Barbeiro = require('./barbeiroModel');

class Atendimento extends Document {

  constructor() {

    super();

    this.idBarbearia = { default : '', type : String};
    this.idBarbeiro = { default : '', type : String};
    this.idUsuario = { default : '', type : String};
    this.senha = { type : Number, required : true };
    this.dthrSolicitacao = { type : Date };
    this.dthrInicio = { type : Date };
    this.dthrFim = { type : Date };
    this.inStatus = { default: 0, type : Number};
    /*
      0 - Pendente
      1 - Em antendimento
      2 - Encerrado
      3 - Cancelado
    */
    this.inZerado = { type : Number, default : 0 }
  }

  verificaPermCanc(idUsuario) {

    return new Promise((resolve, reject) => {

      let permissao = false;

      if (this.idUsuario == idUsuario) return resolve(true);

      return resolve(Barbeiro.verificaPermBarbeiro(this.idBarbearia, idUsuario));
    });
  }

  static getQtSenhasRestantes(idBarbearia, senhaAtual, senhaUsuario, callback) {

    this.count({idBarbearia: idBarbearia, senha:{$gt:parseInt(senhaAtual), $lt:parseInt(senhaUsuario)}, $or:[{inStatus:0},{inStatus:null}], inZerado : 0})
      .then((qtSenhasRestantes) => callback(null, qtSenhasRestantes))
      .catch((err) => callback(err));
  }

  static getSenha(idBarbearia, Ordem) {

    return new Promise((resolve, reject) => {

      let arrayOrdenacao = (Ordem == 'primeira') ? ['senha'] : ['-senha'];

      return this.find({idBarbearia : idBarbearia, $or:[{inStatus:0},{inStatus:null}], inZerado : 0}, {sort: arrayOrdenacao})
        .then(ultAtendimento => resolve(ultAtendimento[0] ? ultAtendimento[0] : null))
        .catch(err => reject(err));
    });
  }

  static getSenhaEmAndamentoBarbeiro(idBarbearia, idBarbeiro) {

    return new Promise((resolve, reject) => {

      return this.findOne({idBarbearia : idBarbearia, idBarbeiro : String(idBarbeiro), inStatus : 1, inZerado : 0})
        .then(atendimento => resolve(atendimento))
        .catch(err => reject(err));
    });
  }
}

module.exports = Atendimento;
