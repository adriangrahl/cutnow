var EmbeddedDocument = require('camo').EmbeddedDocument;
		ObjectId = require('mongodb').ObjectId;

class Localizacao extends EmbeddedDocument {

	constructor(){

		super();

    this.endereco = { default : '', type : String };
    this.cidade = { default : '', type : String };
		this.type = { default : 'Point', type : String};
    this.coordinates = [Number];
	}
}

module.exports = Localizacao;
