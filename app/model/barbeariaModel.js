var Document = require('camo').Document,
		Portfolio = require('./portfolioModel.js'),
		Localizacao = require('./localizacaoModel.js');

class Barbearia extends Document {

	constructor() {

		super();

		this.entidade = String;
		this.email = String;
		this.idAdministrador = String;
		this.portfolio = [Portfolio];
		this.localizacao = Localizacao;
	}

	static verificaPermAdm(barbeariaId, usuarioId) {

		return new Promise((resolve, reject) => {

			this.findOne({_id : barbeariaId})
        .then((barbearia) => {

          if (barbearia == undefined) return resolve(false);

					return (barbearia.idAdministrador == usuarioId) ? resolve(true) : resolve(false);
        })
        .catch((err) => {
          return reject(err);
        });
		});
	}

	deleteImageByID(ID) {

    let index = this.portfolio.findIndex(x => x.ID == ID);

    if (index > -1) this.portfolio.splice(index,1);
  }

	static exists(idBarbearia) {

		return new Promise((resolve, reject) => {

			this.findOne({_id: idBarbearia})
				.then(barbearia => resolve(barbearia!=undefined))
				.catch(err => reject(err));
		});
	}
}

module.exports = Barbearia;
