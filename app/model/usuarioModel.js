//var mongoose = require('mongoose');
var Document = require('camo').Document;
var bcrypt = require('bcrypt-nodejs');

class Usuario extends Document{

  constructor(){

    super();

    this.nome = String;
    this.email = String;
    this.senha = String;
    this.sexo = String;
    this.dataNasc = Date;
  }

	verificaSenha(senha) {

    return new Promise((resolve, reject) => {

      bcrypt.compare(senha, this.senha, function(err, isMatch) {

  			if (err) reject(err);
  			resolve(isMatch);
  		});
    });
	}

	geraHash() {

		return new Promise((resolve, reject) => {

			let usuario = this;

			bcrypt.genSalt(5, function(err, salt) {

				if (err) return reject(err);

				bcrypt.hash(usuario.senha, salt, null, function(err, hash) {

					if (err) return reject(err);
					usuario.senha = hash;
					return resolve(usuario);
				})
			})
		});
	}
}

module.exports = Usuario;//mongoose.model('usuarios', UsuarioSchema);
