var EmbeddedDocument = require('camo').EmbeddedDocument;
		ObjectId = require('mongodb').ObjectId;

class Portfolio extends EmbeddedDocument {

	constructor(){

		super();

    this.ID = { default : '', type : String};
		this.imagem = { default : '', type : String};
		this.descricao = { default : '', type : String};
		this.local = { default : '', type : String};
	}
}

module.exports = Portfolio;
