var Document = require('camo').Document;
var Usuario = require('./usuarioModel');

class Barbeiro extends Usuario {

	constructor() {

		super();

		this.papel = {type: String, default: ""};
    this.idBarbearia = String;
	}

	static verificaPermBarbeiro(barbeariaId, barbeiroId) {

		return new Promise((resolve, reject) => {

			this.findOne({_id : barbeiroId, idBarbearia : barbeariaId})
        .then((barbeiro) => {

          return (barbeiro == undefined) ? resolve(false) : resolve(true);
        })
        .catch((err) => {
          return reject(err);
        });
		});
	}

	static collectionName() {
      return 'usuarios';
  }
}

module.exports = Barbeiro;
