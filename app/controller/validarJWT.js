var Barbeiro = require('../model/barbeiroModel.js');
var jwt = require('jwt-simple');
var segredo = 'projetocutnowapp';

function validaJWT(papel, req, res, callback) {

  var token = (req.body && req.body.access_token) || (req.query && req.query.access_token) || req.headers['x-access-token'];

  if (token) {
    try {

      var decoded = jwt.decode(token, segredo);

      if (decoded.exp <= Date.now()) {
        res.json(403.17, {error: 'Acesso expirado, faça login novamente.'});
      }

      //TODO: não utilizar model de barbeiros para login/validação de token,
      //foi feito desta forma pois o barbeiro possui mais campos
      Barbeiro.findOne({ _id: decoded.iss })
        .then((user) => {

          if (!user) return res.status(401).json({message: 'Token inválido.', err: 'Token inválido.'});

          //Utilizar o decode JWT ao invés dos dados do usuário...
          //quando papel == null é acessível a qualquer usuário, caso contrário é necessário possuir permissão específica
          if (!!papel) {

            var idBarbearia = (req.body && req.body.idBarbearia) || req.query.idBarbearia;

            //Retornar apenas uma mensagem de acesso negado.
            if (!idBarbearia) {
              return res.status(401).json({message: 'faltou idBarbearia fera...'});
            }

            if ((!decoded.idBarbearia) || (decoded.idBarbearia != idBarbearia)) {
              return res.status(401).json({message: 'Acesso negado'});
            }

            if ((!decoded.papel) ||
               ((decoded.papel.toLowerCase() != 'admin') && (decoded.papel.toLowerCase() != papel))) {
              return res.status(401).json({message: 'Permissão insuficiente'});
            }
          }

          callback(user);
        })
        .catch((err) => {
          res.status(500).json({message: "Erro ao procurar usuario do token.", err:err});
        });
    } catch (err) {
      return res.status(500).json({message: 'Erro ao procurar usuario do token.', err: err});
    }
  } else {
    res.status(401).json({message: 'Token não informado', err: 'Token não informado'});
  }
};

module.exports = function(req,res,callback){
  return validaJWT;
}
//ex token seusegredodetoken
//eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiIxIiwiZXhwIjoxNDgzNDkzOTg1MzM2fQ.nA0sPG9NMJbKFn1lrKBlo8LexbF61hwDCJHK_igndK0
