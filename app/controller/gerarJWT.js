var jwt = require('jwt-simple');
var moment = require('moment');
var segredo = 'projetocutnowapp';
var logger = require('../../services/logger');

function geraJWT(usuario) {

	return new Promise((resolve, reject) => {

		try {
			var expires = moment().add(7,'days').valueOf();
			var token = jwt.encode({
				iss: usuario._id,
				exp: expires,
				papel: usuario.papel,
				idBarbearia: usuario.idBarbearia
			}, segredo);

			logger.silly('geraJWT","Gerado token: '+token+'","'+usuario._id);

			return resolve({
				token: token,
				expires: expires,
				user: usuario
			})
		} catch(err) {
			return reject(err);
		}
	});
}

module.exports = function(usuario, callback){
	return geraJWT;
}
