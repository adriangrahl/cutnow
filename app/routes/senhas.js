var Atendimento = require('../model/atendimentoModel');
var Barbearia = require('../model/barbeariaModel');
var validaJWT = require('../controller/validarJWT')();
var logger = require('../../services/logger');
var ObjectId = require('mongodb').ObjectId;
var moment = require('moment');

module.exports = function(app) {

	app.get('/qtSenhasRestantes', function(req, res){
		validaJWT(null, req, res, function(usuario){

			logger.debug('qtSenhasRestantes","Recebida requisição de consulta de quantidade de senhas restantes ","'+usuario._id);
			req.checkQuery('idBarbearia', 'Código da barbearia deve ser informado.').notEmpty();
			req.checkQuery('senhaAtual', 'Senha atual deve ser maior que zero.').isInt().gte(1);
			req.checkQuery('senhaUsuario', 'Senha do usuário deve ser maior que zero.').isInt().gte(1);

			let erros = req.validationErrors();

			if (erros) return res.status(400).json({message:'Foram encontrados erros de validação.',err:erros});

			let idBarbearia  = req.query.idBarbearia,
					senhaAtual   = req.query.senhaAtual,
					senhaUsuario = req.query.senhaUsuario;

			Atendimento.getQtSenhasRestantes(idBarbearia, senhaAtual, senhaUsuario, function(err, qtSenhasRestantes){

				if (err) return res.status(400).json({message:'erro ao consultar senhas restantes.', err: err});

				return res.status(200).json({qtSenhasRestantes: qtSenhasRestantes});
			});
		});
	});

	//consulta última senha não atendida
	app.get('/senhas',function(req, res, next){
    validaJWT(null, req, res, function(usuario){
			logger.debug('senhas","Recebida requisição de consulta de senhas ","'+usuario._id);
			req.checkQuery('idBarbearia', 'Código da barbearia não pode ser vazio.').notEmpty();

			var erros = req.validationErrors();

			if (erros) return res.status(400).json({message:'Foram encontrados erros de validação.',err:erros});

			var barbeariaId = req.query.idBarbearia;

			Barbearia.exists(barbeariaId)
			.then((barbeariaExiste) => {

				if (!barbeariaExiste) return res.status(404).json({message : "Barbearia "+barbeariaId+"não encontrada."});

				Promise.all([
					Atendimento.getSenha(barbeariaId,'primeira'),
					Atendimento.getSenha(barbeariaId,'ultima')]
				).then(senhas => {
					res.status(200).json({atual: senhas[0], ultima : senhas[1]});
				})
				.catch(err => res.status(500).json({err : err}));
			})
			.catch(err => res.status(500).json({message : "Erro ao validar barbearia."}));
		});
	});

	//consulta histórico de atendimentos

	//solicita nova senha
	app.get('/novaSenha',function(req, res, next){
		validaJWT(null, req, res, function(usuario){
			logger.debug('novaSenha","Recebida requisição de novaSenha ","'+usuario._id);

			req.checkQuery('idBarbearia', 'Código da barbearia deve ser informado.').notEmpty();
			var erros = req.validationErrors();

			if (erros) return res.status(400).json({message:'Foram encontrados erros de validação.',err:erros});

			var barbeariaId = req.query.idBarbearia;

			Barbearia.exists(barbeariaId)
			.then((barbeariaExiste) => {

				if (!barbeariaExiste) return res.status(404).json({message : "Barbearia "+barbeariaId+" não encontrada."});

				//busca a última senha de determinada barbearia, a partir do maiorID de atendimento
				Atendimento.find({idBarbearia:barbeariaId, inZerado : 0}, {sort: ['-senha']})
				.then(ultAtendimento => {

					let novaSenha;
					(ultAtendimento[0] == undefined)  ? (novaSenha = 1) : (novaSenha = parseInt(ultAtendimento[0].senha,10)+1);

					let novoAtendimento = Atendimento.create({
						idBarbearia : String(barbeariaId),
						idUsuario : String(usuario._id),
						senha : novaSenha,
						dthrSolicitacao : new Date().toISOString()
					});

					novoAtendimento.save()
					.then(result => res.status(201).json({message: 'Senha gerada: '+novoAtendimento.senha, data: novoAtendimento}))
					.catch(err => res.status(500).json({message:'erro ao criar senha para a barbearia: '+barbeariaId,err:err}));
				})
				.catch(err => res.status(500).json({message:'erro ao consultar senha atual da barbearia: '+barbeariaId,err:err}));
			})
			.catch(erro => res.status(500).json({message : "Erro ao validar barbearia "+barbeariaId}));
		});
	});

	//inicia o atendimento
	app.post('/atenderSenha',function(req, res, next){
		validaJWT("barbeiro", req, res, function(usuario){

			logger.debug('iniciarAtendimento","Recebida requisição de início de atendimento ","'+usuario._id);

			req.checkBody('idBarbearia', 'Código da barbearia deve ser informado.').notEmpty();
			req.checkBody('senha', 'Número da senha deve ser maior que zero.').isInt().gte(1);

			var erros = req.validationErrors();

			if (erros) return res.status(400).json({message:'Foram encontrados erros de validação.',err:erros});

			var senhaId = req.body.senha;

			Atendimento.getSenhaEmAndamentoBarbeiro(usuario.idBarbearia, usuario._id)
				.then(atendBarbeiro => {

					if (atendBarbeiro != undefined) return res.status(400).json({message:'Este Barbeiro já está em atendimento', atendimento: atendBarbeiro});

					Atendimento.findOne({idBarbearia : usuario.idBarbearia, senha : parseInt(senhaId), inZerado : 0},{sort:['-senha']})
						.then(atendimento=>{

							if (atendimento == undefined) return res.status(400).json({message:'Senha não encontrada.'});

							atendimento.idBarbeiro = String(usuario._id);
							atendimento.inStatus = 1; //atendimento iniciado
							atendimento.dthrInicio = new Date().toISOString();

							atendimento.save()
								.then(retorno => {

									res.status(200).json({message: 'Senha '+senhaId+', atendimento iniciado.', data: retorno})
								})
								.catch(err => res.status(500).json({message:'Erro inesperado ao iniciar atendimento.',err:err}))
						})
						.catch(err=> res.status(500).json({message:'erro ao consultar senha: '+senhaId+' da barbearia '+barbeariaId, erro:err}))
				})
				.catch(err => res.status(500).json({message: 'Erro ao verificar se barbeiro está em atendimento'}));
		});
	});

	//encerra o atendimento
	app.post('/encerrarSenha',function(req, res, next){
		validaJWT("barbeiro", req, res, function(usuario){
			logger.debug('atenderSenha","Recebida requisição de atenderSenha ","'+usuario._id);

			req.checkBody('idBarbearia', 'Código da barbearia deve ser informado.').notEmpty();
			req.checkBody('senha', 'Número da senha deve ser maior que zero.').isInt().gte(1);

			var erros = req.validationErrors();

			if (erros) return res.status(400).json({message:'Foram encontrados erros de validação.',err:erros});

			var barbeariaId = req.body.idBarbearia;
			var senhaId = req.body.senha;

			Atendimento.findOne({idBarbearia:barbeariaId, senha:parseInt(senhaId), inZerado : 0},{sort:['-senha']})
				.then(atendimento=>{

					if (atendimento == undefined) return res.status(400).json({message:'Senha não encontrada.'});

					if (atendimento.inStatus != 1) return res.status(400).json({message:'É necessário primeiramente iniciar o atendimento.'});

					if ((usuario.papel.toLowerCase() != 'admin') && (atendimento.idBarbeiro != usuario._id))
						return res.status(400).json({message:'Somente o barbeiro que iniciou o atendimento pode encerrá-lo.'});

					atendimento.inStatus = 2; //encerrado
					atendimento.dthrFim = new Date().toISOString();

					atendimento.save()
						.then(retorno => {

							res.status(200).json({message: 'Senha '+senhaId+' atendida.', data: retorno})
						})
						.catch(err => res.status(500).json({message:'Erro inesperado ao atender senha.',err:err}))
				})
				.catch(err=> res.status(500).json({message:'erro ao consultar senha: '+senhaId+' da barbearia '+barbeariaId, erro:err}))
		});
	});

	app.post('/cancelarSenha',function(req, res, next){
		validaJWT(null, req, res, function(usuario){
			logger.debug('cancelarSenha","Recebida requisição de cancelarSenha ","'+usuario._id);

			req.checkBody('idBarbearia', 'Código da barbearia deve ser informado.').notEmpty();
			req.checkBody('senha', 'Número da senha deve ser maior que zero.').isInt().gte(1);

			var erros = req.validationErrors();

			if (erros) return res.status(400).json({message:'Foram encontrados erros de validação.',err:erros});

			var barbeariaId = req.body.idBarbearia;
			var senhaId = req.body.senha;

			Barbearia.exists(barbeariaId)
				.then(barbeariaExiste => {

					if (!barbeariaExiste) return res.status(404).json({message : "Barbearia "+barbeariaId+" não encontrada."});

					Atendimento.findOne({idBarbearia:barbeariaId, senha:parseInt(senhaId), inZerado : 0})
						.then(atendimento => {

							if (atendimento == undefined) return res.status(400).json({message:'Senha não encontrada.',err:'Senha não encontrada.'});

							if (atendimento.inStatus == 1 ) return res.status(400).json({message:'Esta senha já está em atendimento.'});
							if (atendimento.inStatus == 3 ) return res.status(200).json({message:'Esta senha já foi cancelada.'});

							atendimento.verificaPermCanc(usuario._id)
								.then(ok => {

									if (!ok) return res.status(400).json({message:'Usuário não possui permissão para cancelar esta senha.'});

									atendimento.inStatus = 3; //cancelado

									atendimento.save()
										.then(retorno => res.status(200).json({message: 'Senha '+senhaId+' cancelada.', data: retorno}))
										.catch(err => res.status(500).json({message:'erro ao cancelar senha.',err:err}));
								})
								.catch(err => res.status(500).json({message:'erro ao consultar permissão de cancelamento. Usuario:'+usuario._id+' Barbearia:'+barbeariaId+' senha:'+senhaId,err:err}))
						})
						.catch(err=> res.status(500).json({message:'erro ao consultar senha: '+senhaId+' da barbearia '+barbeariaId, erro:err}))
				})
				.catch(err=> res.status(500).json({message:'erro ao consultar barbearia: '+barbeariaId}));
		});
	});

	app.get('/senhaEmAndamentoBarbeiro',function(req, res, next){
    validaJWT("barbeiro", req, res, function(usuario){
			logger.debug('senhaEmAndamentoBarbeiro","Recebida requisição de consulta de senhas em and para o barbeiro","'+usuario._id);
			req.checkQuery('idBarbeiro', 'Código do barbeiro não pode ser nulo.').notEmpty();

			var erros = req.validationErrors();

			if (erros) return res.status(400).json({message:'Foram encontrados erros de validação.',err:erros});

			var barbeiroId = req.query.idBarbeiro;

			Atendimento.getSenhaEmAndamentoBarbeiro(usuario.idBarbearia, barbeiroId)
				.then(atendimento => res.status(200).json({atendimento : atendimento}))
				.catch(err => res.status(500).json({ message : 'erro ao consultar atendimento atual do barbeiro '+usuario._id, err : err }))
		});
	});

	//fila
	app.get('/fila',function(req, res, next){
    validaJWT("barbeiro", req, res, function(usuario){
			logger.debug('fila","Recebida requisição de consulta de fila da barbearia","'+usuario._id);
			req.checkQuery('idBarbearia', 'Código do barbearia deve ser informada.').notEmpty();

			var erros = req.validationErrors();

			if (erros) return res.status(400).json({message:'Foram encontrados erros de validação.',err:erros});

			Atendimento.find({idBarbearia : req.query.idBarbearia, inZerado : 0},{sort: ['senha']})
				.then(atendimentos => res.status(200).json({atendimentos : atendimentos}))
				.catch(err => res.status(500).json({message: 'Erro ao consultar fila. Barbearia: '+req.query.idBarbearia, err: err}));
		});
	});

	app.get('/historico',function(req, res, next){
    validaJWT("barbeiro", req, res, function(usuario){
			logger.debug('historico","Recebida requisição de consulta de historico da barbearia","'+usuario._id);
			req.checkQuery('idBarbearia', 'Código do barbearia deve ser informada.').notEmpty();

			var erros = req.validationErrors();

			if (erros) return res.status(400).json({message:'Foram encontrados erros de validação.',err:erros});

			Atendimento.find({idBarbearia : req.query.idBarbearia, inZerado : 1},{sort: ['-senha']})
				.then(atendimentos => res.status(200).json({atendimentos : atendimentos}))
				.catch(err => res.status(500).json({message: 'Erro ao consultar histórico. Barbearia: '+req.query.idBarbearia, err: err}));
		});
	});

	app.get('/zerarSenhas',function(req, res, next){
    validaJWT("admin", req, res, function(usuario){
			logger.debug('zerarSenhas","Recebida requisição para zerar senhas","'+usuario._id);
			req.checkQuery('idBarbearia', 'Código do barbearia deve ser informada.').notEmpty();

			var erros = req.validationErrors();

			if (erros) return res.status(400).json({message:'Foram encontrados erros de validação.',err:erros});

			Atendimento.find({idBarbearia : req.query.idBarbearia, $or:[{inStatus:0},{inStatus:null},{inStatus:1}], inZerado : 0})
				.then(atendimentoEmAberto => {

					if (atendimentoEmAberto.length > 0) return res.status(400).json({message: "Para zerar as senhas não podem haver atendimentos pendentes ou em andamento.", atendimento: atendimentoEmAberto});

					return Atendimento.find({idBarbearia : req.query.idBarbearia, inZerado : 0});
				})
				.then((atendimentos) => {

					var data = moment(new Date().toISOString()).format("YYYY-MM-DD");

					if ((atendimentos.length > 0) &&
							(moment(atendimentos[atendimentos.length-1].dthrSolicitacao).format("YYYY-MM-DD")) == data){

						console.log('data atual:');
						console.log(data);

						console.log('ultima senha dthrSolicitacao:');
						console.log(moment(atendimentos[atendimentos.length-1].dthrSolicitacao).format("YYYY-MM-DD"));

						return res.status(400).json({message:'As senhas podem ser zeradas somente uma vez ao dia.', ultimaVez: moment(atendimentos[atendimentos.length-1].dthrSolicitacao).format("DD/MM/YYYY")});
					}

					atendimentos.map((atendimento) => {

						atendimento.inZerado = 1;

						atendimento.save()
							.catch(err => res.status(500).json({message:'Erro ao zerar atendimento.', atendimento : atendimento, err:err}));
					});

					return res.status(200).json({message : "Atendimentos zerados com sucesso.", atendimentosZerados : atendimentos});
				})
				.catch(err => res.status(500).json({message: 'Erro ao tentar zerar atendimentos da barbearia: '+req.query.idBarbearia, err: err}));
		});
	});
}
