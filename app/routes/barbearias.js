var Barbearia = require('../model/barbeariaModel');
var validaJWT = require('../controller/validarJWT')();
var logger = require('../../services/logger');

module.exports = function(app) {

	app.get('/barbearias',function(req, res, next){
		validaJWT(null, req, res, function(usuario){

			logger.debug('barbearias","Recebida requisição de consulta de barbearias.","'+usuario._id);

			Barbearia.find({})
				.then((barbearias) => {
					return res.status(200).json({message:'Barbearias disponíveis',data:barbearias});
				})
				.catch((err) => {
					return res.status(500).json({err: err});
				});
		});
	});

	app.post('/novaBarbearia',function(req, res, next){

		if (process.env.NODE_ENV != 'test')
			return res.status(400).json({message: 'Este recurso só pode ser acessado em ambiente de teste.'});

		let dados = req.body;

		let novaBarbearia = Barbearia.create({
			entidade : dados.entidade//,
			//localizacao : dados.localizacao,
			//portfolio : dados.portfolio
		})
		.save()
		.then((barbeariaCriada) => res.status(201).json({barbearia:barbeariaCriada}))
		.catch((err) => res.status(500).json({err: err}));
	});

	app.post('/setaAdminBarbearia',function(req, res, next){

		if (process.env.NODE_ENV != 'test')
			return res.status(400).json({message: 'Este recurso só pode ser acessado em ambiente de teste.'});

		let dados = req.body;

		Barbearia.findOne({_id : dados.idBarbearia})
			.then((barbearia) => {

				if (!barbearia) return res.status(400).json({message: 'Barbearia não encontrada (test only).'});

				barbearia.idAdministrador = dados.idAdministrador;

				barbearia.save()
					.then((ok) => res.status(200).json({message: 'Setado admin da barbearia (test only).'}))
					.catch((err) => res.status(500).json({err: err}));

			})
			.catch((err) => res.status(500).json({message: 'Erro ao buscar barbearia (test only).', err: err}));
	});
}
