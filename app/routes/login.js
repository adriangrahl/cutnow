//var Usuario = require('../model/usuarioModel');
var Barbeiro = require('../model/barbeiroModel');
var jwt = require('jwt-simple');
var moment = require('moment');
var segredo = 'seusegredodetoken';
var gerarToken = require('../controller/gerarJWT')();

module.exports = function(app) {

	app.post('/login',function(req,res,next){

		req.assert('email','Email é obrigatório!').notEmpty();
		req.assert('senha','Senha deve possuir entre 6 e 20 caracteres').len(6, 20);

		let erros = req.validationErrors();

		if (erros) return res.status(400).json({message:'Foram encontrados erros de validação.',err:erros});

		let usuario = req.body;

		//utilizado barbeiro para conseguir acesso ao id, caso houver.
		Barbeiro.findOne({"email":usuario.email})
      .then((user) => {

        if (!user) return res.sendStatus(401);

        user.verificaSenha(usuario.senha)
          .then((isMatch) => {

            if (!isMatch) return res.sendStatus(401);

            gerarToken(user)
							.then(token=> res.status(200).json(token))
							.catch(err => res.status(500).json(err));
          })
          .catch((err) => {

            return res.status(500).json({err: err});
          });
      })
      .catch((err) => {
        return res.status(500).json({err: err});
      });
	});
}
//geração de token
/*var expires = moment().add(7,'days').valueOf();
  var token = jwt.encode({
    iss: usuario.id,
    exp: expires
  }, segredo);

  console.log('token gerado: '+token);

return res.status(200).json({
  message: 'bem vindo '+user.nome,
    token : token,
    expires: expires,
    user: user.toJSON()
});*/
