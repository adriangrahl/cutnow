var Barbeiro = require('../model/barbeiroModel.js');
var validaJWT = require('../controller/validarJWT')();
var gerarToken = require('../controller/gerarJWT')();
var moment = require('moment');
var logger = require('../../services/logger');

module.exports = function(app) {

	app.post('/novoBarbeiro',function(req,res,next){
		validaJWT("admin", req, res, function(usuario){

			logger.debug('novoBarbeiro","Recebida requisição de novoBarbeiro');

			var barbeiro = req.body;

			req.assert('nome','Nome é obrigatório!').notEmpty();
			req.assert('email','Email é obrigatório!').notEmpty();
	    req.assert('idBarbearia','Barbearia é obrigatória!').notEmpty();
			req.assert('senha', 'Senha deve possuir entre 6 e 20 caracteres').len(6, 20);
			req.assert('sexo','Sexo é obrigatório!').notEmpty();
			req.assert('dataNasc','Data de nascimento é obrigatória!').notEmpty();

			var erros = req.validationErrors();

			if (erros) return res.status(400).json({message:'Foram encontrados erros de validação.',err:erros});

			var dateFormat = "YYYY-MM-DD";
			var dataValida = moment(barbeiro.dataNasc, dateFormat, true).isValid();

			if (!dataValida) return res.status(400).json({message:'Foram encontrados erros de validação.',err:"Data de nascimento inválida", value: barbeiro.dataNasc});

			Barbeiro.findOne({email:barbeiro.email})
				.then((result) => {

					if (result) return res.status(400).json({message: 'Email já existente.', err: barbeiro.email});

					logger.debug('novobarbeiro","Vai criar novo barbeiro');

					let novoBarbeiro = Barbeiro.create({
						idBarbearia: barbeiro.idBarbearia,
						nome: barbeiro.nome,
						email: barbeiro.email,
						senha: barbeiro.senha,
						sexo: barbeiro.sexo,
						dataNasc: new Date(barbeiro.dataNasc).toISOString(),
						papel: "barbeiro"
					});

					novoBarbeiro.geraHash()
					.then(retorno => retorno.save())
					.then(gerarToken)
					.then(token => res.status(201).json({message: 'Barbeiro cadastrado com sucesso.', data: token}))
					.catch((err) => {
						 return  res.status(500).json({message: 'Erro ao gravar Barbeiro.', err: err})
					});
				})
				.catch((err) => {
					return res.status(500).json({message: 'Erro ao consultar Barbeiro.', err: err})
				});
		});
	});

  app.get('/barbeiros',function(req, res, next){
		validaJWT(null, req, res, function(usuario){

      console.log('/barbeiros');
			logger.debug('barbeiros","Recebida requisição de consulta de barbeiros.","'+usuario._id);

			req.checkQuery('idBarbearia', 'Código da barbearia deve ser informado.').notEmpty();
			var erros = req.validationErrors();

			if (erros) return res.status(400).json({message:'Foram encontrados erros de validação.',err:erros});

			var barbeariaId = req.query.idBarbearia;

			Barbeiro.find({idBarbearia: barbeariaId})
				.then((barbeiros) => {
					return res.status(200).json({message:'Barbeiros vinculados', data : barbeiros });
				})
				.catch((err) => {
					return res.status(500).json({err: err});
				});
		});
	});

	//por enquanto fixo
	// app.post('/permissoesBarbeiro', function(req, res, next) {
	// 	validaJWT(req, res, function(usuario){
	//
  //     console.log('/permissoesBarbeiro');
	// 		logger.debug('permissoesBarbeiro","Recebida requisição de alteração de permissões do barbeiro.","'+usuario._id);
	//
	// 		req.checkQuery('idBarbearia', 'Código da barbearia deve ser informado.').notEmpty();
	// 		var erros = req.validationErrors();
	//
	// 		if (erros) return res.status(400).json({message:'Foram encontrados erros de validação.',err:erros});
	//
	// 		var barbeariaId = req.query.idBarbearia;
	//
  //     console.log('barbeariaId = '+barbeariaId);
	//
	// 		Barbeiro.find({})
	// 			.then((barbeiros) => {
	// 				return res.status(200).json({message:'Barbeiros vinculados', data : barbeiros });
	// 			})
	// 			.catch((err) => {
	// 				return res.status(500).json({err: err});
	// 			});
	// 	});
	// });

	app.post('/novoAdmin',function(req, res, next){

		if (process.env.NODE_ENV != 'test')
			return res.status(400).json({message: 'Este recurso só pode ser acessado em ambiente de teste.'});

		let dados = req.body;

		let novoAdmin = Barbeiro.create({
			idBarbearia: dados.idBarbearia,
			nome: dados.nome,
			email: dados.email,
			senha: dados.senha,
			sexo: dados.sexo,
			dataNasc: new Date(dados.dataNasc).toISOString(),
			papel: "admin"
		});

		novoAdmin.geraHash()
		.then(retorno => retorno.save())
		.then(gerarToken)
		.then(token => res.status(201).json({message: 'Admin cadastrado com sucesso (test only).', data: token}))
		.catch((err) => {
			 return  res.status(500).json({message: 'Erro ao gravar Admin (test only).', err: err})
		});
	});
}
