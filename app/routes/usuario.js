var Usuario = require('../model/usuarioModel.js');
var validaJWT = require('../controller/validarJWT');
var gerarToken = require('../controller/gerarJWT')();
var moment = require('moment');
var logger = require('../../services/logger');

module.exports = function(app) {

	app.post('/novoUsuario',function(req,res,next){
		logger.debug('novoUsuario","Recebida requisição de novoUsuario');

		var usuario = req.body;

		req.assert('nome','Nome é obrigatório!').notEmpty();
		req.assert('email','Email é obrigatório!').notEmpty();
		req.assert('senha', 'Senha deve possuir entre 6 e 20 caracteres').len(6, 20);
		req.assert('sexo','Sexo é obrigatório!').notEmpty();
		req.assert('dataNasc','Data de nascimento é obrigatória!').notEmpty();

		var erros = req.validationErrors();

		if (erros) return res.status(400).json({message:'Foram encontrados erros de validação.',err:erros});

		var dateFormat = "YYYY-MM-DD";
		var dataValida = moment(usuario.dataNasc, dateFormat, true).isValid();

		if (!dataValida) return res.status(400).json({message:'Foram encontrados erros de validação.',err:"Data de nascimento inválida", value: usuario.dataNasc});

		Usuario.findOne({email:usuario.email})
			.then((result) => {

				if (result) return res.status(400).json({message: 'Email já existente.', err: usuario.email});

				logger.debug('novoUsuario","Vai criar novo usuario');

				let novoUsuario = Usuario.create({
					nome: usuario.nome,
					email: usuario.email,
					senha: usuario.senha,
					sexo: usuario.sexo,
					dataNasc: new Date(usuario.dataNasc).toISOString()
				});

				novoUsuario.geraHash()
				.then(retorno => retorno.save())
				.then(gerarToken)
				.then(token => res.status(201).json({message: 'Usuário cadastrado com sucesso.', data: token}))
				.catch((err) => {
					 console.log(err);
					 return res.status(500).json({message: 'Erro ao gravar usuário.', err: err})
			  });
			})
			.catch((err) => {
				console.log(err);
				return res.status(500).json({message: 'Erro ao consultar usuário.', err: err})
			});
	});

	// app.get('/testePapel',function(req,res,next){
	// 	validaJWT(null, req, res, function(usuario){
	//
	// 		console.log('conseguiu acesso.');
	// 		return res.status(200).json({message: "ok"});
	// 	});
	// });
}
