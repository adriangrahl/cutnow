var Barbearia = require('../model/barbeariaModel');
var Portfolio = require('../model/portfolioModel');
var validaJWT = require('../controller/validarJWT')();
var logger = require('../../services/logger');
var fs = require('fs');
var utils = require('../../services/utils');
var ObjectId = require('mongodb').ObjectId;

module.exports = function(app) {

	app.get('/portfolio',function(req, res, next){
		validaJWT(null, req, res, function(usuario){

			logger.debug('getPortfolio","Recebida requisição de consulta de portfolio.","'+usuario._id);
			req.checkQuery('idBarbearia', 'Código da barbearia não pode ser nulo.').notEmpty();

			let erros = req.validationErrors();

			if (erros) return res.status(400).json({message:'Foram encontrados erros de validação.',err:erros});

			let barbeariaId = req.query.idBarbearia;

			Barbearia.findOne({_id : barbeariaId})
        .then((barbearia) => {

          if (!barbearia) return res.status(400).json({message:'Barbearia não encontrada.'});

          let i = 0;
  				let imagens = [];

  				while (i <= barbearia.portfolio.length-1) {

  					imagem = {
  						ID : barbearia.portfolio[i].ID,
  						nome : barbearia.portfolio[i].imagem,
  						descricao : barbearia.portfolio[i].descricao,
  						local : barbearia.portfolio[i].local
  					}

  					imagens.push(imagem);
  					i++;
  				}

  				return res.status(200).json(imagens);
        })
        .catch((err) => {
          if (err) return res.status(500).json({message:'Erro ao consultar barbearia.', err: err});
        });
		});
	});

	app.post('/portfolio',function(req, res, next){
		validaJWT("barbeiro", req, res, function(usuario){

			logger.debug('postPortfolio","Recebida requisição de inserção de img portfolio.","'+usuario._id);
			req.checkQuery('idBarbearia', 'Código da barbearia não pode ser nulo.').notEmpty();

			let erros = req.validationErrors();

			if (erros) return res.status(400).json({message:'Foram encontrados erros de validação.',err:erros});

			let barbeariaId = req.query.idBarbearia;
			let descricao = req.query.descricao;
			let filename = req.headers.filename;

			Barbearia.findOne({_id:barbeariaId})
        .then((barbearia) => {

          if (!barbearia) return res.status(400).json({message:'Barbearia não encontrada.'});

          Barbearia.verificaPermAdm(barbeariaId, usuario._id)
            .then((ok) =>{

              if (!ok) return res.status(400).json({message:'Usuário não possui permissão.'});

              fs.mkdir(utils.getDirImagens()+'/'+barbearia._id, function () {
                //nesse caso não é possível receber o req.body, pois é um Stream (chunk)
                //portanto será criado novamente o streamWriter com o fs já implementado padrão pelo express (req)
                req.pipe(fs.createWriteStream(utils.getDirImagens()+'/'+barbearia._id+'/'+filename))
                .on('finish', function(){

                  let imagem = Portfolio.create({
										ID: String(ObjectId()),
                    imagem: filename,
                    descricao: descricao,
                    local: utils.getDirImagens()+'/'+barbearia._id+'/'+filename
                  });

                  barbearia.portfolio.push(imagem);

                  barbearia.save()
                    .then((retorno) => {
                      return res.status(201).json({message:'Imagem gravada com sucesso'});
                    })
                    .catch((err) => {
                      return res.status(500).json({message:'Erro inesperado ao gravar imagem.',err:err});
                    });
                });
              });
            });
        });
		});
	});

	app.delete('/portfolio',function(req, res, next){
		validaJWT("barbeiro", req, res, function(usuario){
			logger.debug('deletePortfolio","Recebida requisição de deleção de imagem do portfolio.","'+usuario._id);
			req.checkQuery('idBarbearia', 'Código da barbearia deve ser maior que zero.').notEmpty();
			req.checkQuery('idImg', 'Id da imagem deve ser informado').notEmpty();

			var erros = req.validationErrors();

			if (erros) {
				return res.status(400).json({message:'Foram encontrados erros de validação.',err:erros});
			}

			var barbeariaId = req.query.idBarbearia;
			var idImg = req.query.idImg;

			Barbearia.findOne({_id : barbeariaId})
				.then((barbearia) => {

					barbearia.deleteImageByID(idImg);

					barbearia.save()
						.then((ok) => res.status(200).json({message: 'Imagem removida com sucesso.'}))
						.catch((err) => res.sendStatus(500).json({err: err}));
				})
				.catch((err) => res.sendStatus(500).json({err: err}));
		});
	});
}
