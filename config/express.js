//carrega para a aplicação os endpoints
var express = require('express');
var load = require('express-load');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
//var mongoose = require('mongoose');

var db;
var production = 'mongodb://admin:123456@127.0.0.1:27017/barbearia?authSource=admin';
var test = 'mongodb://admin:123456@127.0.0.1:27017/test?authSource=admin';
var connect = require('camo').connect;
var database;

var dirImagens = '';

module.exports = function() {
	var app = express();
	//var router = express.Router();

	//app.use('/api', router);

	// router.route('/usuarios')
    //.get(require('./validarJWT'), rotas.getUsuarios)
    //.post(rotas.postUsuarios);

	//router.route('/login')
    //	.post(rotas.login);

	//__dirname //export NODE_ENV=production

	if (process.env.NODE_ENV == 'production') {
		console.log('ambiente produção');
		dirImagens = '/root/Barbearia/images';
	} else {
		console.log('ambiente local');
		dirImagens = 'E:/Adrian/Alura/JavaScript/Barbearia/images';
	}
		app.use("/images", express.static(dirImagens));

	app.set('view engine','ejs');
	app.set('views','./app/views');

	//middleware body-parser, que intercepta a req/res
	//para Content-type: 'text/html'
	app.use(bodyParser.urlencoded({extended:true}));
	//para Content-type: 'application/json'
	app.use(bodyParser.json());
	//app.use(expressValidator()); //sem custoValidators
	app.use(expressValidator({     //com custoValidators
		customValidators:{
	    	gte: function(param, num) {
	        	return param >= num;
	    	}
	 	}
	}));

	load('routes',{cwd: 'app'}) //carregue tudo que estiver em rotas, pasta a partir da qual deve procurar
		//.then('model') //depois tudo que estiver em infra
		.then('controller')
		.into(app); //para dentro do express

	/*app.use(function(req,res,next){
		res.status(404).render('erros/404');
		next();
	});*/

	/*app.use(function(error,req,res,next){
		if (process.env.NODE_ENV == 'production') {
			res.status(500).render('erros/500');
			return;
		}
		next(error);//se for ambiente dev, deixa o express seguir o caminho normal com a mensagem de erro, mostrando em tela.
	});*/

	//conecta ao banco mongoDB com o mongoose
	/*if (process.env.NODE_ENV == 'test')
		mongoose.connect(test)
	else
		mongoose.connect(db);*/

		if (process.env.NODE_ENV == 'test'){
			console.log('conexão: TEST');
			db = connect(test, { poolSize: 10 });
		} else {
			console.log('conexão: PRODUCTION');
			db = connect(production, { poolSize: 10 });
		}

		db.then((conexao) => {
		    console.log('Conexão camo realizada com sucesso.');
		    database = conexao;
		  })
			.catch((err) => {
				console.log('Erro ao conectar com o banco (camo). Err: '+err);
				process.exit(1);
			});

	return app;
}
