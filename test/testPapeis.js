var assert = require('assert');
var express = require('../config/express')();
var request = require('supertest')(express);
var usrModel = require('../app/model/usuarioModel');
var connect = require('camo').connect;
var tokenAdrian = '',
    tokenDiogo = '';

console.log('node_env = '+process.env.NODE_ENV);

if (process.env.NODE_ENV != 'test')
	process.exit();

class testPapeisModel {

  constructor() {

    this.idBarbearia = "";
    this.idUsuario = "";
    this.token = "";
    this.senha = "";
  }
}


//Limpeza deve ser realizada manualmente
describe('testPapeis',function(){

  //ESTE PRIMEIRO AGUARDA A CONEXAO CAMO
  it('#novoUsuarioFiller',function(done){
		var dadosInputados =
		{
			'nome' : 'filler',
			'email' : 'filler1@hotmail.com',
			'senha' : '123456',
			'sexo' : 'm',
			'dataNasc' : '1993-07-04'
		};

		request.post('/novoUsuario')
        .set('Accept','application/json')
        .send(dadosInputados)
        .end(function(err, res){
        	done();
        });
	});

  it('#novoUsuarioFiller2',function(done){
		var dadosInputados =
		{
			'nome' : 'filler2',
			'email' : 'filler2@hotmail.com',
			'senha' : '123456',
			'sexo' : 'm',
			'dataNasc' : '1993-07-04'
		};

		request.post('/novoUsuario')
        .set('Accept','application/json')
        .send(dadosInputados)
        .end(function(err, res){
        	done();
        });
	});

  it('#novoUsuarioFiller3',function(done){
		var dadosInputados =
		{
			'nome' : 'filler3',
			'email' : 'filler3@hotmail.com',
			'senha' : '123456',
			'sexo' : 'm',
			'dataNasc' : '1993-07-04'
		};

		request.post('/novoUsuario')
        .set('Accept','application/json')
        .send(dadosInputados)
        .end(function(err, res){
        	done();
        });
	});

  it('#novoUsuarioFiller4',function(done){
		var dadosInputados =
		{
			'nome' : 'filler4',
			'email' : 'filler2@hotmail.com',
			'senha' : '123456',
			'sexo' : 'm',
			'dataNasc' : '1993-07-04'
		};

		request.post('/novoUsuario')
        .set('Accept','application/json')
        .send(dadosInputados)
        .end(function(err, res){
        	done();
        });
	});

  it('#novoUsuarioFiller5',function(done){
		var dadosInputados =
		{
			'nome' : 'filler5',
			'email' : 'filler5@hotmail.com',
			'senha' : '123456',
			'sexo' : 'm',
			'dataNasc' : '1993-07-04'
		};

		request.post('/novoUsuario')
        .set('Accept','application/json')
        .send(dadosInputados)
        .end(function(err, res){
        	done();
        });
	});

  //****************************************************************************

  var usuarioCliente = new testPapeisModel();

	it('#novoUsuarioCliente',function(done){
		var dadosInputados =
		{
			'nome' : 'Adrian Cliente teste papel',
			'email' : 'adrian.cliente@hotmail.com',
			'senha' : '123456',
			'sexo' : 'm',
			'dataNasc' : '1993-07-04'
		};

		request.post('/novoUsuario')
      .set('Accept','application/json')
      .send(dadosInputados)
      //.expect('res.status',201)
      .end(function(err, res){
        assert.equal(res.status,201);
        usuarioCliente.idUsuario = res.body.data.user._id;
        usuarioCliente.token = res.body.data.token;
      	done();
      });
	});

  var barbeariaAdrian = new testPapeisModel();

  it('#novaBarbeariaAdrian',function(done){
		var dadosInputados =
		{
			'entidade' : 'Barbearia do Adrian Admin',
		};

		request.post('/novaBarbearia')
      .set('Accept','application/json')
      .send(dadosInputados)
      //.expect('res.status',201)
      .end(function(err, res){
        assert.equal(res.status,201);
        barbeariaAdrian.idBarbearia = res.body.barbearia._id;

      	done();
      });
	});

  it('#novoUsuarioAdmin',function(done){
		var dadosInputados =
		{
			'nome' : 'Adrian Admin teste papel',
			'email' : 'adrian.admin@hotmail.com',
			'senha' : '123456',
			'sexo' : 'm',
			'dataNasc' : '1993-07-04',
      'idBarbearia' : barbeariaAdrian.idBarbearia
		};

		request.post('/novoAdmin')
      .set('Accept','application/json')
      .send(dadosInputados)
      .end(function(err, res){
        assert.equal(res.status,201);
        barbeariaAdrian.idUsuario = res.body.data.user._id;
        barbeariaAdrian.token = res.body.data.token;
      	done();
      });
	});

  it('#setaAdminBarbeariaAdrian',function(done){
		var dadosInputados =
		{
			'idAdministrador' : barbeariaAdrian.idUsuario,
			'idBarbearia' : barbeariaAdrian.idBarbearia
		};

		request.post('/setaAdminBarbearia')
      .set('Accept','application/json')
      .send(dadosInputados)
      .end(function(err, res){
        assert.equal(res.status,200);
      	done();
      });
	});

  it('#UsuarioCriaNovoBarbeiroFalhaPermissao',function(done){
		var dadosInputados =
		{
			'idBarbearia' : barbeariaAdrian.idBarbearia,
      'nome':'Adrian Barbeiro',
      'email':'adrian.barbeiro@hotmail.com',
      'sexo':'m',
      'dataNasc':'1993-07-04'
		};

		request.post('/novoBarbeiro')
      .set('Accept','application/json')
      .set('x-access-token',usuarioCliente.token)
      .send(dadosInputados)
      .end(function(err, res){
        assert.equal(res.status,401);
      	done();
      });
	});

  var barbeiroAdrian = new testPapeisModel();

  it('#novoBarbeiro',function(done){
		var dadosInputados =
		{
			'idBarbearia' : barbeariaAdrian.idBarbearia,
      'nome':'Adrian Barbeiro',
      'email':'adrian.barbeiro@hotmail.com',
      'senha':'123456',
      'sexo':'m',
      'dataNasc':'1993-07-04'
		};

		request.post('/novoBarbeiro')
      .set('Accept','application/json')
      .set('x-access-token',barbeariaAdrian.token)
      .send(dadosInputados)
      .end(function(err, res){
        assert.equal(res.status,201);
        barbeiroAdrian.idUsuario = res.body.data.user._id;
        barbeiroAdrian.token = res.body.data.token;
        barbeiroAdrian.idBarbearia = barbeariaAdrian.idBarbearia;
      	done();
      });
	});

  //Barbearia do Diogo
  var barbeariaDiogo = new testPapeisModel();

  it('#novaBarbeariaDiogo',function(done){
		var dadosInputados =
		{
			'entidade' : 'Barbearia do Diogo Admin',
		};

		request.post('/novaBarbearia')
      .set('Accept','application/json')
      .send(dadosInputados)
      .end(function(err, res){
        assert.equal(res.status,201);
        barbeariaDiogo.idBarbearia = res.body.barbearia._id;
      	done();
      });
	});

  it('#novoUsuarioAdmin',function(done){
		var dadosInputados =
		{
			'nome' : 'Diogo Admin teste papel',
			'email' : 'diogo.admin@hotmail.com',
			'senha' : '123456',
			'sexo' : 'm',
			'dataNasc' : '1993-07-04',
      'idBarbearia' : barbeariaDiogo.idBarbearia
		};

		request.post('/novoAdmin')
      .set('Accept','application/json')
      .send(dadosInputados)
      .end(function(err, res){
        assert.equal(res.status,201);
        barbeariaDiogo.idUsuario = res.body.data.user._id;
        barbeariaDiogo.token = res.body.data.token;
      	done();
      });
	});

  it('#setaAdminBarbeariaDiogo',function(done){
		var dadosInputados =
		{
			'idAdministrador' : barbeariaDiogo.idUsuario,
			'idBarbearia' : barbeariaDiogo.idBarbearia
		};

		request.post('/setaAdminBarbearia')
      .set('Accept','application/json')
      .send(dadosInputados)
      .end(function(err, res){
        assert.equal(res.status,200);
      	done();
      });
	});

  var barbeiroDiogo = new testPapeisModel();

  //Criada barbearia Adrian, Barbearia Diogo, Usuário cliente, Barbeiro Adrian e Barbeiro Diogo
  it('#novoBarbeiroDiogo',function(done){
		var dadosInputados =
		{
			'idBarbearia' : barbeariaDiogo.idBarbearia,
      'nome':'Diogo Barbeiro',
      'email':'diogo.barbeiro@hotmail.com',
      'senha':'123456',
      'sexo':'m',
      'dataNasc':'1993-07-04'
		};

		request.post('/novoBarbeiro')
      .set('Accept','application/json')
      .set('x-access-token',barbeariaDiogo.token)
      .send(dadosInputados)
      .end(function(err, res){
        assert.equal(res.status,201);
        barbeiroDiogo.idUsuario = res.body.data.user._id;
        barbeiroDiogo.token = res.body.data.token;
        barbeiroDiogo.idBarbearia = barbeariaAdrian.idBarbearia;
      	done();
      });
	});

  it('#novaSenhaClienteBarbeariaAdrian',function(done){
		var dadosInputados = 'idBarbearia='+barbeariaAdrian.idBarbearia;

		request.get('/novaSenha?'+dadosInputados)
        .set('Accept','application/json')
        .set('x-access-token',usuarioCliente.token)
        .expect('Content-Type',/json/)
        .end(function(err, res){
        	assert.equal(res.status,201);
          barbeariaAdrian.senha = res.body.data.senha;
        	done();
        });
	});

  it('#atenderSenhaSemPermissaoBarbeiroDiogoBarbeariaAdrian',function(done){
		var dadosInputados =
		{
			idBarbearia : barbeariaAdrian.idBarbearia,
			senha : barbeariaAdrian.senha
		};

		request.post('/atenderSenha')
        .set('Accept','application/json')
        .send(dadosInputados)
        .set('x-access-token',barbeiroDiogo.token)
        .expect('Content-Type',/json/)
        .end(function(err, res){
        	assert.equal(res.status,401);
        	assert.equal(JSON.stringify(res.body.message).indexOf('negado') != -1, true);//deve conter a mensagem de não possui permissão
        	done();
        });
	});

  it('#atenderSenhaSemPermissaoUsuarioBarbeariaAdrian',function(done){
		var dadosInputados =
		{
			idBarbearia : barbeariaAdrian.idBarbearia,
			senha : barbeariaAdrian.senha
		};

		request.post('/atenderSenha')
        .set('Accept','application/json')
        .send(dadosInputados)
        .set('x-access-token',usuarioCliente.token)
        .expect('Content-Type',/json/)
        .end(function(err, res){
        	assert.equal(res.status,401);
        	assert.equal(JSON.stringify(res.body.message).indexOf('negado') != -1, true);//deve conter a mensagem de não possui permissão
        	done();
        });
	});

  it('#atenderSenhaSemPermissaoDiogoBarbeariaAdrian',function(done){
		var dadosInputados =
		{
			idBarbearia : barbeariaAdrian.idBarbearia,
			senha : barbeariaAdrian.senha
		};

		request.post('/atenderSenha')
        .set('Accept','application/json')
        .send(dadosInputados)
        .set('x-access-token',barbeiroDiogo.token)
        .expect('Content-Type',/json/)
        .end(function(err, res){
        	assert.equal(res.status,401);
        	assert.equal(JSON.stringify(res.body.message).indexOf('negado') != -1, true);//deve conter a mensagem de não possui permissão
        	done();
        });
	});

  it('#atenderSenhaSemPermissaoDiogoAdminBarbeariaAdrian',function(done){
		var dadosInputados =
		{
			idBarbearia : barbeariaAdrian.idBarbearia,
			senha : barbeariaAdrian.senha
		};

		request.post('/atenderSenha')
        .set('Accept','application/json')
        .send(dadosInputados)
        .set('x-access-token',barbeariaDiogo.token)
        .expect('Content-Type',/json/)
        .end(function(err, res){
        	assert.equal(res.status,401);
        	assert.equal(JSON.stringify(res.body.message).indexOf('negado') != -1, true);//deve conter a mensagem de não possui permissão
        	done();
        });
	});

  it('#atenderComPermissaoBarbeariaAdrian',function(done){
		var dadosInputados =
		{
			idBarbearia : barbeariaAdrian.idBarbearia,
			senha : barbeariaAdrian.senha
		};

		request.post('/atenderSenha')
        .set('Accept','application/json')
        .send(dadosInputados)
        .set('x-access-token',barbeiroAdrian.token)
        .expect('Content-Type',/json/)
        .end(function(err, res){
        	assert.equal(res.status,200);
        	done();
        });
	});

  it('#atenderComPermissaoAdminBarbeariaAdrian',function(done){
		var dadosInputados =
		{
			idBarbearia : barbeariaAdrian.idBarbearia,
			senha : barbeariaAdrian.senha
		};

		request.post('/atenderSenha')
        .set('Accept','application/json')
        .send(dadosInputados)
        .set('x-access-token',barbeariaAdrian.token)
        .expect('Content-Type',/json/)
        .end(function(err, res){
        	assert.equal(res.status,200);
        	done();
        });
	});
});
