var assert = require('assert');
var express = require('../config/express')();
var request = require('supertest')(express);
var usrModel = require('../app/model/usuarioModel');
var connect = require('camo').connect;
var tokenAdrian = '',
    tokenDiogo = '';

console.log('node_env = '+process.env.NODE_ENV);

if (process.env.NODE_ENV != 'test')
	process.exit();

// describe('LimpezaBanco', function(){
// 	it('#dropUsuarios',function(done){
// 		mongoose.connection.collections['usuarios'].drop(function(err){
// 			done();
// 		});
// 	});
//
// 	/*it('#dropBarbearias',function(done){
// 		mongoose.connection.collections['barbearias'].drop(function(err){
// 			done();
// 		});
// 	});*/
//
// 	it('#dropAtendimentos',function(done){
// 		mongoose.connection.collections['atendimentos'].drop(function(err){
// 			done();
// 		});
// 	});
// });

//Limpeza deve ser realizada manualmente
describe('testUsuario',function(){

  //ESTE PRIMEIRO AGUARDA A CONEXAO CAMO
	it('#novoUsuario',function(done){
		var dadosInputados =
		{
			'nome' : 'adrian',
			'email' : 'adrian@hotmail.com',
			'senha' : '123456',
			'sexo' : 'm',
			'dataNasc' : '1993-07-04'
		};

		request.post('/novoUsuario')
        .set('Accept','application/json')
        .send(dadosInputados)
        .expect('status',201)
        .end(function(err, res){
        	done();
        });
	});

  // it('#novoUsuario Adrian',function(done){
	// 	var dadosInputados =
	// 	{
	// 		'nome' : 'Adrian',
	// 		'email' : 'adrian@hotmail.com',
	// 		'senha' : '123456',
	// 		'sexo' : 'm',
	// 		'dataNasc' : '1993-07-04'
	// 	};
  //
	// 	request.post('/novoUsuario')
  //       .set('Accept','application/json')
  //       .send(dadosInputados)
  //       .end(function(err, res){
  //         assert.equal(res.status,201);
  //       	done();
  //       });
	// });
  //
	// it('#novoUsuario Diogo',function(done){
	// 	var dadosInputados =
	// 	{
	// 		'nome' : 'Diogo',
	// 		'email' : 'diogo@hotmail.com',
	// 		'senha' : '123456',
	// 		'sexo' : 'm',
	// 		'dataNasc' : '1991-05-27'
	// 	};
  //
	// 	request.post('/novoUsuario')
  //       .set('Accept','application/json')
  //       .send(dadosInputados)
  //       .expect('Content-Type',/json/)
  //       .end(function(err, res){
  //         assert.equal(res.status,201);
  //       	done();
  //       });
	// });
  //
  // it('#novoUsuario já existente',function(done){
	// 	var dadosInputados =
	// 	{
	// 		'nome' : 'Diogo',
	// 		'email' : 'diogo@hotmail.com',
	// 		'senha' : '123456',
	// 		'sexo' : 'm',
	// 		'dataNasc' : '1991-05-27'
	// 	};
  //
	// 	request.post('/novoUsuario')
  //       .set('Accept','application/json')
  //       .send(dadosInputados)
  //       .expect('Content-Type',/json/)
  //       .end(function(err, res){
  //         assert.equal(res.status,400);
  //       	done();
  //       });
	// });
});

describe('testLogin', function(){

	it('#LoginSucessoAdrian',function(done){
		var dadosInputados =
		{
			'email' : 'adrian@hotmail.com',
			'senha' : '123456'
		};

		request.post('/login')
        .set('Accept','application/json')
        .send(dadosInputados)
        .expect('Content-Type',/json/)
        .end(function(err, res){

        	assert.equal(res.status,200);
        	tokenAdrian = res.body.token;
        	done();
        });
	});

  it('#LoginErroAdrian',function(done){
		var dadosInputados =
		{
			'email' : 'adrian@hotmail.com',
			'senha' : '1234567'
		};

		request.post('/login')
        .set('Accept','application/json')
        .send(dadosInputados)
        .expect('Content-Type',/json/)
        .end(function(err, res){
        	assert.equal(res.status,401);
        	done();
        });
	});

	it('#LoginSucessoDiogo',function(done){
		var dadosInputados =
		{
			'email' : 'diogo@hotmail.com',
			'senha' : '123456'
		};

		request.post('/login')
        .set('Accept','application/json')
        .send(dadosInputados)
        .expect('Content-Type',/json/)
        .end(function(err, res){
        	assert.equal(res.status,200);
        	tokenDiogo = res.body.token;
        	done();
        });
	});
});

describe('testBarbearias', function(){
	it('#Barbearias',function(done){
		request.get('/barbearias')
        .set('Accept','application/json')
        .set('x-access-token',tokenAdrian)
        .expect('Content-Type',/json/)
        .end(function(err, res){
        	assert.equal(res.status,200);
        	done();
        });
	});
});

describe('testSenhas', function(){
	it('#novaSenhaAdrian',function(done){
		var dadosInputados = 'idBarbearia=58dc4bde1380359ca50d95e9';//1

		request.get('/novaSenha?'+dadosInputados)
        .set('Accept','application/json')
        .set('x-access-token',tokenAdrian)
        .expect('Content-Type',/json/)
        .end(function(err, res){
        	assert.equal(res.status,201);
        	//console.log('Senha 1 adrian= '+JSON.stringify(res.body.data));
        	done();
        });
	});

	it('#novaSenhaAdrian',function(done){
		var dadosInputados = 'idBarbearia=58dc4bde1380359ca50d95e9';//1

		request.get('/novaSenha?'+dadosInputados)
        .set('Accept','application/json')
        .set('x-access-token',tokenAdrian)
        .expect('Content-Type',/json/)
        .end(function(err, res){
        	assert.equal(res.status,201);
        	//console.log('Senha 2 adrian= '+JSON.stringify(res.body.data));
        	done();
        });
	});

	it('#novaSenhaDiogo',function(done){
		var dadosInputados = 'idBarbearia=58dc4bde1380359ca50d95e9';//1

		request.get('/novaSenha?'+dadosInputados)
        .set('Accept','application/json')
        .set('x-access-token',tokenDiogo)
        .expect('Content-Type',/json/)
        .end(function(err, res){
        	assert.equal(res.status,201);
        	done();
        });
	});

	it('#novaSenhaDiogo',function(done){
		var dadosInputados = 'idBarbearia=58dc4bea13803589ca50d95ea';//2

		request.get('/novaSenha?'+dadosInputados)
        .set('Accept','application/json')
        .set('x-access-token',tokenDiogo)
        .expect('Content-Type',/json/)
        .end(function(err, res){
        	assert.equal(res.status,201);
        	done();
        });
	});

	it('#novaSenhaErro',function(done){
		var dadosInputados = 'idBarbearia=3';

		request.get('/novaSenha?'+dadosInputados)
        .set('Accept','application/json')
        .set('x-access-token',tokenDiogo)
        .expect('Content-Type',/json/)
        .end(function(err, res){
        	assert.equal(res.status,400);
        	done();
        });
	});

	it('#senhas',function(done){
		var dadosInputados = 'idBarbearia=58dc4bde1380359ca50d95e9';

		request.get('/senhas?'+dadosInputados)
        .set('Accept','application/json')
        .set('x-access-token',tokenAdrian)
        .expect('Content-Type',/json/)
        .end(function(err, res){
        	assert.equal(res.status,200);
        	//console.log('res.body = '+JSON.stringify(res.body));
        	done();
        });
	});

	it('#atenderSenhaSemPermissao',function(done){
		var dadosInputados =
		{
			idBarbearia : '58dc4bde1380359ca50d95e9',//1
			senha : '1'
		};

		request.post('/atenderSenha')
        .set('Accept','application/json')
        .send(dadosInputados)
        .set('x-access-token',tokenDiogo)
        .expect('Content-Type',/json/)
        .end(function(err, res){
        	assert.equal(res.status,400);
        	assert.equal(JSON.stringify(res.body.message).indexOf('permiss') != -1, true);//deve conter a mensagem de não possui permissão
        	done();
        });
	});

	it('#atenderSenhaSemBarbearia',function(done){
		var dadosInputados =
		{
			idBarbearia : '3',
			senha : '1'
		};

		request.post('/atenderSenha')
        .set('Accept','application/json')
        .send(dadosInputados)
        .set('x-access-token',tokenAdrian)
        .expect('Content-Type',/json/)
        .end(function(err, res){
        	assert.equal(res.status,400);
        	assert.equal(JSON.stringify(res.body.message).indexOf('Barbearia não') != -1, true);//deve conter a mensagem de não possui permissão
        	//console.log('res.body.message = '+JSON.stringify(res.body.message));
        	done();
        });
	});

	it('#atenderSenhaComPermissao',function(done){
		var dadosInputados =
		{
			idBarbearia : '58dc4bde1380359ca50d95e9',//1
			senha : '1'
		};

		request.post('/atenderSenha')
        .set('Accept','application/json')
        .send(dadosInputados)
        .set('x-access-token',tokenAdrian)
        .expect('Content-Type',/json/)
        .end(function(err, res){
        	assert.equal(res.status,200);
        	//console.log('res.body.message = '+JSON.stringify(res.body.message));
        	//console.log('res.body.data = '+JSON.stringify(res.body.data));
        	done();
        });
	});

	it('#cancelarSenhaSemBarbearia',function(done){
		var dadosInputados =
		{
			idBarbearia : '3',
			senha : '1'
		};

		request.post('/cancelarSenha')
        .set('Accept','application/json')
        .send(dadosInputados)
        .set('x-access-token',tokenAdrian)
        .expect('Content-Type',/json/)
        .end(function(err, res){
        	assert.equal(res.status,400);
        	assert.equal(JSON.stringify(res.body.message).indexOf('Barbearia não') != -1, true);
        	done();
        });
	});

	it('#cancelarSenhaComPermAdm',function(done){
		var dadosInputados =
		{
			idBarbearia : '58dc4bde1380359ca50d95e9',//1
			senha : '1'
		};

		request.post('/cancelarSenha')
        .set('Accept','application/json')
        .send(dadosInputados)
        .set('x-access-token',tokenAdrian)
        .expect('Content-Type',/json/)
        .end(function(err, res){
        	assert.equal(res.status,200);
        	//assert.equal(JSON.stringify(res.body.message).indexOf('permissão para cancelamento') != -1, true);
        	done();
        });
	});

	it('#cancelarSenhaSemPermissaoUsuario',function(done){
		var dadosInputados =
		{
      idBarbearia:'58dc4bde1380359ca50d95e9',
	    senha:'2'
		};

		request.post('/cancelarSenha')
        .set('Accept','application/json')
        .send(dadosInputados)
        .set('x-access-token',tokenDiogo)
        .expect('Content-Type',/json/)
        .end(function(err, res){
        	assert.equal(res.status,400);
        	assert.equal(JSON.stringify(res.body.message).indexOf('permissão para cancelar') != -1, true);
        	done();
        });
	});
});
